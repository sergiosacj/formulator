\documentclass[a4paper, 12pt]{article}

\usepackage[brazil,english]{babel}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{hyperref}
\usepackage{textgreek}
\usepackage{ragged2e}
\usepackage[T1]{fontenc}
\usepackage[user,titleref]{zref}
\usepackage[top=2.5cm,bottom=2.5cm,left=2.5cm,right=2.5cm]{geometry}
\usepackage{longtable}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[markup=underlined]{changes}
%\usepackage[final]{changes}

% Identação do Parágrafo
\RequirePackage{indentfirst}
\parindent 1.25cm

% Identação rodapé
\addtolength{\footnotesep}{5mm}
\renewcommand{\thefootnote}{\textbf{\arabic{footnote}}}

% Formatação de estilos do package algorithm
\renewcommand{\algorithmicrequire}{\textbf{Entrada:}}
\renewcommand{\algorithmicensure}{\textbf{Sa\'{i}da:}}
\renewcommand{\algorithmicdo}{\textbf{fa\c{c}a}}
\renewcommand{\algorithmicend}{\textbf{fim}}
\renewcommand{\algorithmicif}{\textbf{se}}
\renewcommand{\algorithmicthen}{\textbf{ent\~{a}o}}
\renewcommand{\algorithmicelse}{\textbf{sen\~{a}o}}
\renewcommand{\algorithmicwhile}{\textbf{enquanto}}
\renewcommand{\algorithmicfor}{\textbf{para}}
\renewcommand{\algorithmicforall}{\textbf{para todo}}
\renewcommand{\algorithmicloop}{\textbf{loop}}
\renewcommand{\algorithmicrepeat}{\textbf{repita}}
\renewcommand{\algorithmicuntil}{\textbf{at\'{e}}}
\newcommand{\captionALG}[2]{\centerline{ \textbf{Algoritmo #2: \textbf{#1}} }}

% Minimize operator
\DeclareMathOperator*{\minimize}{Minimizar}

% Horizontal Line
\newcommand{\drawline}[1][.2pt]{\par\vskip.5\baselineskip\hrule height #1\par\vskip.5\baselineskip}

% Custom Label
\makeatletter
\newcommand{\customlabel}[2]{\protected@write \@auxout {}{\string \newlabel {#1}{{#2}{}}}}
\makeatother

% Teoremas
\newtheorem{definicao}{Definição}
\newtheorem{lema}{Lema}
\newtheorem{corolario}{Corolário}
\newtheorem{teorema}{Teorema}

\begin{document}

\pagestyle{headings}

\title{Abordagem pseudo-Booleana para consolidação de máquinas virtuais}
\author{
Eliseu Kadesh\\
Maicon Lucas Mares\\
Sérgio de Almeida Cipriano\\
Thiago Guilherme\\
Victor Jorge Gonçalves\\
{\normalsize Faculdade do Gama $\cdot$ Universidade de Brasília}
}
\date{}

\selectlanguage{brazil}
\thispagestyle{empty}

\maketitle

%\begin{abstract}
%    
%    \beginription}
%    	\item[Palavras-chave] .
%    \endription}
%\end{abstract}

\section{Introdução}

Apoiando-se no problema de consolidação de máquinas virtuais apresentado por
\cite{ribas2013}, surgiu a necessidade de se verificar o impacto causado pela
introdução de uma nova variante chamada \emph{overbooking} na resolução desses
problemas. O \emph{overbooking} é definido pelo o ato de ultrapassar
deliberadamente o número de recursos dos \emph{hardwares} envolvidos no
problema por uma margem escolhida arbitrária, de tal forma que possibilite a
satisfatibilidade (SAT) de casos que antes não eram SAT devido à falta de
recursos.

Assim, a consolidação de máquinas virtuais é um problema cuja instância é
descrita por uma lista de \emph{hardwares} (HWs) e \emph{virtual machines}
(VMs), onde cada \emph{hardware} $HW_{i}$ possui uma quantidade arbitrária de
CPUs $HW^{cpu}_{i}$ e de memória RAM $HW^{ram}_{i}$; e, cada \emph{virtual
machine} $VM_{j}$ érita por uma quantidade arbitrária CPUs $VM^{cpu}_{j}$
e de memória RAM $VM^{ram}_{i}$. Em uma instância do problema de consolidação
de máquinas virtuais, o objetivo é encontrar uma relação de distribuição das
\emph{virtual machines} sobre o conjunto de \emph{hardwares} disponíveis,
garantindo que a quantidade de \emph{hardwares} ligados seja mínima, e que
nenhuma \emph{virtual machine} fique sem ser alocada em um \emph{hardware}
ligado.

Neste trabalho, o objetivo é realizar testes para o caso de \emph{overbooking},
que é definido pelo aumento deliberado de recurso de CPU em cada
\emph{hardware} fornecido no problema. Como apontado anteriormente, o recurso
de CPU de cada \emph{hardware} será acrescido de um valor arbitrário, neste
caso, como um fator de porcentagem K, gerando um novo valor
$HWI_{i}^{cpu} = HW_{i}^{cpu} * (1 + K)$. Os objetivos da problemática com
\emph{overbooking} permanecem identicos aos da problemática original,
mudando-se apenas o valor de CPUs a serem considerados na sua resolução.

\section{Metodologia}

A experimentação numérica é a metodologia utilizada nesse trabalho, com a
análise de dados quantitativos. Para realizar esses experimentos foi utilizado
o conjunto de arquivos de teste fornecidos em \cite{ribas2013}. Com isso, foi
desenvolvido um programa, em Python3, para gerar o conjunto de restrições do
problema. Esse programa faz a leitura de um arquivo contendo informações sobre
os \textit{hardwares} e as máquinas virtuais. Os códigos desenvolvidos e demais
arquivos produzidos para os experimentos estão disponíveis no
GitLab\footnote{https://gitlab.com/sergiosacj/formulator}.

Os testes foram executados utilizando o \textit{solver} Clasp
\cite{gebser2012conflict} na máquina \textbf{gpu1}, disponibilizada na
infraestrutura Chococino, com um tempo limite de 300 segundos.

\section{Desenvolvimento}

O objetivo deste Capítulo é apresentar as formulações pseudo-booleanas
empregadas para a solução do problema proposto. O Capítulo está divido em duas
partes, na primeira é apresentado a formulação desenvolvida no trabalho
\cite{ribas2013} e na segunda parte é discorrido a formulação com alterações
seguindo a variante \textit{overbooking} já explicada anteriormente. O objetivo
de cada uma dessas formulações é a resolução do problema de distribuição de
máquinas virtuais de modo a gerar o menor número possível de \textit{hardwares}
ativos.

\subsection{Modelo Base proposto}
A formulação proposta em \cite{ribas2013} contém as seguintes variáveis:
\begin{itemize}
    \item N: quantidade de \textit{hardwares} (hw);
    \item K: quantidade de máquinas virtuais a serem alocadas (VM);
    \item $hw_i$: \textit{hardware i} $\in N$;
    \item ${vm_j}^{hw_i}$: máquina virtual \textit{j} rodando em determinado \textit{hardware i}.
\end{itemize}

Temos $N$ variáveis querevem a quantidade de \textit{hardwares} e $K$
máquinas virtuais relacionadas aos $N$ \textit{hardwares}, isso nos dá um total
de $(N + N$ X $K)$ variáveis. As restrições presentes nessa formulação são:

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \displaystyle \min & : & \sum_{i=1}^N hw_i  \\
    \end{array}
    \label{eq:objective}
\end{equation}

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \sum_{i=1}^N R_{hw_i} \cdot hw_i & \geq & \sum_{j=1}^K R_{vm_j}
    \end{array}
    \label{eq:hwram}
\end{equation}

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \sum_{i=1}^N P_{hw_i} \cdot hw_i & \geq & \sum_{j=1}^K P_{vm_j}
    \end{array}
    \label{eq:hwcpu}
\end{equation}

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \forall i \in 1..N (\sum_{j=1}^K (R_{vm_j} \cdot \neg vm_{j}^{hw_i}) + R_{hw_i} \cdot hw_i \geq \sum_{j=1}^K R_{vm_j})
    \end{array}
    \label{eq:hwramrestr}
\end{equation}

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \forall i \in 1..N (\sum_{j=1}^K (P_{vm_j} \cdot \neg vm_{j}^{hw_i}) + P_{hw_i} \cdot hw_i \geq \sum_{j=1}^K P_{vm_j})
    \end{array}
    \label{eq:hwcpurestr}
\end{equation}

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \forall j \in 1..K (\sum_{i=1}^N vm_j^{hw_i} \geq 1)
    \end{array}
    \label{eq:vmminrun}
\end{equation}

\begin{equation}
    \setlength{\arraycolsep}{2pt}
    \begin{array}{lcr}
         \forall j \in 1..K (\sum_{i=1}^N \neg vm_j^{hw_i} \geq N-1)
    \end{array}
    \label{eq:vmmaxrun}
\end{equation}

A Equação \eqref{eq:objective}reve o objetivo do modelo que é minimizar a
quantidade de hardwares ativos. As equações \eqref{eq:hwram} e \eqref{eq:hwcpu}
descrevem que a quantidade de RAM e CPU disponíveis de todos os hardwares deve
ser suficiente para as necessidades das máquinas virtuais, respectivamente.

% Acrescentar código para essas equações e explica

Já as equações \eqref{eq:hwramrestr} e \eqref{eq:hwcpurestr} determinam que os
recursos de cada \textit{hardware}, RAM e CPU, devem ser suficientes para o
total de recursos necessários para todas as máquinas que estejam rodando em
cada um deles. O lado esquerdo de cada uma das equações, $(\sum_{j=1}^K
(R_{vm_j} \cdot \neg vm_{j}^{hw_i})$ e $(\sum_{j=1}^K (P_{vm_j} \cdot \neg
vm_{j}^{hw_i})$, eliminam da quantificação as máquinas que não estão rodando
naquele determinado \textit{hardware}, assim a quantificação leva em conta
somente as máquinas que estejam rodando no \textit{hardware} em questão. 

% Acrescentar código para essas equações e explica

A penúltima equação, Equação \eqref{eq:vmminrun}, define que cada máquina
virtual deve rodar em no mínimo um \textit{hardware}, enquanto a última,
Equação \eqref{eq:vmmaxrun}, limita que cada máquina não deve rodar em, pelo
menos, $N-1$ \textit{hardwares}. Em conjunto, essas duas últimas equações
definem que cada máquina pode rodar em
somente um hardware.

\subsection{Formulação com \textit{overbooking}}

A estratégia de \textit{overbooking} empregada consiste em definir um valor
arbitrário e aplicar esse valor para todos os \textit{hardwares} disponíveis no
problema. Por padrão, é considerado um valor $OVERBOOKING = 1$ para a variável
de ambiente que controla a aplicação da estratégia. Nota-se que o valor $1$
significa distribuição de 100\% da CPU disponível, ou seja, quando não é
aplicado o \textit{overbooking}, já valores $OVERBOOKING > 1$ são valores acima
de 100\%, logo, definem a aplicação do \textit{overbooking}.

\section{Experimentação Numérica}

Os testes foram divididos em 2 partes. A Tabela \ref{table1} apresenta
resultado dos experimentos sem o uso de \textit{overbooking}. A Tabela
\ref{table2} e a Tabela 3 \ref{table3} apresentam resultados dos experimentos
com o uso de \textit{overbooking} para valores variados.

\begin{table}
  \caption{Resultados dos experimentos sem \textit{overbooking}}
  \label{table1}
  \begin{center}
    \begin{tabular}{|c|c|}
      \hline
      \textbf{Problema} & \textbf{Tempo (seg)} \\
      \hline
        hw32-vm50p	& 0.029s   \\
        hw32-vm75p	& 1.105s   \\
        hw32-vm85p	& 1.137s   \\
        hw32-vm90p	& 1.125s   \\
        hw32-vm98p	& 3.383s   \\
        hw32-vm99p	& 6.295s   \\
        hw32-vm100p	& 298.001s \\
        hw64-vm25p	& 0.545s   \\
        hw64-vm50p	& 1.518s   \\
        hw64-vm75p	& 2.173s   \\
        hw64-vm85p	& 3.631s   \\
        hw64-vm90p	& 3.584s   \\
        hw64-vm98p	& 4.582s   \\
        hw64-vm99p	& 16.951s  \\
        hw64-vm100p	& 337.660s \\
        hw128-vm25p	& 5.472s   \\
        hw128-vm50p	& 5.555s   \\
        hw128-vm75p	& 7.242s   \\
        hw128-vm85p	& 6.793s   \\
        hw128-vm90p	& 10.475s  \\
        hw128-vm98p	& 17.444s  \\
        hw128-vm99p	& 335.682s \\
        hw256-vm25p	& 10.830s  \\
        hw256-vm50p	& 9.842s   \\
        hw256-vm75p	& 23.538s  \\
        hw256-vm85p	& 29.289s  \\
        hw256-vm90p	& 29.736s  \\
        hw256-vm98p	& 39.842s  \\
        hw256-vm99p	& 340.027s \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[ht]
  \caption{Resultados dos experimentos com \textit{overbooking} de 1.2}
  \label{table2}
  \hspace{-2cm}
  \begin{minipage}[b]{0.1\linewidth}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Tempo (seg)} & \textbf{Problema} & \textbf{Overbooking} \\
      \hline
      9.029s	& hw32-vm99p	& 1.2 \\
      3.568s	& hw64-vm75p	& 1.2 \\
      3.582s	& hw64-vm95p	& 1.2 \\
      67.506s	& hw256-vm98p	& 1.2 \\
      TLE	& hw64-vm100p	& 1.2 \\
      TLE	& hw512-vm99p	& 1.2 \\
      1.178s	& hw32-vm85p	& 1.2 \\
      0.026s	& hw64-vm110p	& 1.2 \\
      9.895s	& hw256-vm25p	& 1.2 \\
      0.317s	& hw32-vm25p	& 1.2 \\
      1.909s	& hw64-vm50p	& 1.2 \\
      1.037s	& hw32-vm75p	& 1.2 \\
      238.274s	& hw512-vm98p	& 1.2 \\
      TLE	& hw128-vm99p	& 1.2 \\
      77.482s	& hw512-vm85p	& 1.2 \\
      4.816s	& hw64-vm98p	& 1.2 \\
      4.317s	& hw64-vm85p	& 1.2 \\
      3.286s	& hw32-vm98p	& 1.2 \\
      86.788s	& hw512-vm90p	& 1.2 \\
      6.772s	& hw128-vm85p	& 1.2 \\
      1.108s	& hw32-vm90p	& 1.2 \\
      0.568s	& hw64-vm25p	& 1.2 \\
      45.072s	& hw512-vm75p	& 1.2 \\
      3.393s	& hw128-vm25p	& 1.2 \\
      8.416s	& hw128-vm95p	& 1.2 \\
      \vdots & \vdots & \vdots \\
      \hline
    \end{tabular}
  \end{minipage}
  \hspace{8cm}
  \begin{minipage}[b]{0.1\linewidth}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{Tempo (seg)} & \textbf{Problema} & \textbf{Overbooking} \\
      \hline
      \vdots & \vdots & \vdots \\
      25.335s	& hw256-vm75p	& 1.2 \\
      0.088s	& hw128-vm110p	& 1.2 \\
      36.009s	& hw512-vm50p	& 1.2 \\
      2.340s	& hw32-vm95p	& 1.2 \\
      7.455s	& hw128-vm75p	& 1.2 \\
      19.322s	& hw128-vm98p	& 1.2 \\
      8.431s	& hw64-vm99p	& 1.2 \\
      10.552s	& hw128-vm90p	& 1.2 \\
      142.623s	& hw512-vm95p	& 1.2 \\
      1.256s	& hw512-vm110p	& 1.2 \\
      TLE	& hw512-vm100p	& 1.2 \\
      TLE	& hw256-vm99p	& 1.2 \\
      29.998s	& hw256-vm95p	& 1.2 \\
      28.562s	& hw256-vm85p	& 1.2 \\
      11.891s	& hw256-vm50p	& 1.2 \\
      0.434s	& hw256-vm110p	& 1.2 \\
      TLE	& hw128-vm100p	& 1.2 \\
      4.027s	& hw64-vm90p	& 1.2 \\
      27.421s	& hw256-vm90p	& 1.2 \\
      TLE	& hw32-vm100p	& 1.2 \\
      0.033s	& hw32-vm50p	& 1.2 \\
      23.383s	& hw512-vm25p	& 1.2 \\
      0.011s	& hw32-vm110p	& 1.2 \\
      6.619s	& hw128-vm50p	& 1.2 \\
      TLE	& hw256-vm100p	& 1.2 \\
      \hline
    \end{tabular}
  \end{minipage}
\end{table}

\begin{table}
  \caption{Resultados dos experimentos com \textit{overbooking} de 1.5}
  \label{table3}
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      \textbf{HW} & \textbf{VM} & \textbf{Tempo (seg)} \\
      \hline
      89.525s	& hw512-vm95p	& 1.5 \\
      1.252s	& hw512-vm110p	& 1.5 \\
      299.998s	& hw512-vm100p	& 1.5 \\
      198.429s	& hw256-vm99p	& 1.5 \\
      33.717s	& hw256-vm95p	& 1.5 \\
      21.646s	& hw256-vm85p	& 1.5 \\
      11.065s	& hw256-vm50p	& 1.5 \\
      0.313s	& hw256-vm110p	& 1.5 \\
      299.998s	& hw128-vm100p	& 1.5 \\
      3.958s	& hw64-vm90p	& 1.5 \\
      28.112s	& hw256-vm90p	& 1.5 \\
      299.998s	& hw32-vm100p	& 1.5 \\
      0.033s	& hw32-vm50p	& 1.5 \\
      13.729s	& hw512-vm25p	& 1.5 \\
      0.013s	& hw32-vm110p	& 1.5 \\
      5.597s	& hw128-vm50p	& 1.5 \\
      299.998s	& hw256-vm100p	& 1.5 \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\section{Análise dos resultados}

\subsection{Modelo Base}

Tendo como referência a Tabela \ref{table1}, é nítido que ao aumentar o número
de VMs a serem alocadas em uma quantidade fixa de \textit{hardwares} resulta no
aumento do tempo necessário para resolução do problema pelo \textit{solver}
Clasp. Isso se deve ao \textit{solver} necessitar de mais iterações para
encontrar uma combinação adequada de recursos disponíveis nos
\textit{hardwares} que atendam às necessidades das VMs, já que os recursos a
serem consumidos por essas tornam-se mais disputados.

\subsection{Overbooking}

Com a introdução da variante \textit{overbooking} a oferta de CPU cresce por
parte dos \textit{hardwares}. Na Tabela \ref{table2} é notável a diminuição do
tempo necessário para encontrar uma combinação adequada que resolva o problema
ao compararmos as mesmas entradas na Tabela \ref{table1}. Conforme aumentamos o
\textit{overbooking} esse tempo tende a reduzir, a causa de tal efeito é o
aumento dos recursos de CPU disponíveis nos \textit{hardwares}. Logo, um número
de iterações menor é necessário para alocar as VMs, já que agora os recursos de
CPU estão sendo menos disputados devido a maior oferta dos mesmos.

\section{Conclusão}

Com os códigos desenvolvidos neste trabalho
\footnote{https://gitlab.com/sergiosacj/formulator} é possível realizar
experimentos mais aprofundados em trabalhos futuros. Foram desenvolvidos
scripts para gerar fórmulas aceitas pelo clasp, interpretar os resultados que o
clasp exibe, gerar tabelas HTML para cada teste específico e executar
automaticamente experimentos dado um conjunto de entradas. Enfim, os artefatos
gerados são livres e distribuídos sob a licença GPLv3.

%% BIBLIOGRAFIA %%
\bibliography{main}
\bibliographystyle{plain-port}

\end{document}

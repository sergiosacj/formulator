import os


def read_ram_cpu():
    n = int(input())
    ram = []
    cpu = []
    for _ in range(n):
        _, rami, cpui = input().split()
        ram.append(int(rami))
        cpu.append(int(int(cpui)*overbooking))
    return n, ram, cpu


def hardware_resources(hw, vm_sum):
    constraint = ""
    for i in range(n):
        constraint += f"+{hw[i]} x{i+1} "
        if i == n-1:
            constraint += f">= {vm_sum};"
    print(constraint)


def hardware_limit(hw, vm, vmsum, variables):
    constraint = ""
    for i in range(n):
        for j in range(k):
            constraint += f"+{vm[j]} ~x{variables[j][i]} "
        constraint += f"+{hw[i]} x{i+1} >= {vmsum};\n"
    print(constraint, end='')


key = 'OVERBOOKING'
if key in os.environ:
    overbooking = float(os.environ[key])
else:
    overbooking = 1
n, hwram, hwcpu = read_ram_cpu()
k, vmram, vmcpu = read_ram_cpu()
vmram_sum = sum(vmram)
vmcpu_sum = sum(vmcpu)

# Variáveis e restrições
print(f"* #variable= {n + n * k} #constraint= {2 + 2 * n + 2 * k}")

print('* formula objetivo que visa reduzir o numero de hardware ligados.')
constraint = "min: "
for i in range(n):
    constraint += f"+1 x{i+1}"
    constraint += ';' if i == n-1 else ' '
print(constraint)


print('* somatorio dos recursos dos hardware ligados são suficientes')
print('* para acomodar os recursos utilizados pelas maquinas virtuais.')
hardware_resources(hwram, vmram_sum)
hardware_resources(hwcpu, vmcpu_sum)

print('* limitacao de recursos por hardware.')
variables = [list(range(1 + n * i, 1 + n * (i + 1))) for i in range(k)]
for i in range(n):
    for j in range(k):
        variables[j][i] += n

hardware_limit(hwram, vmram, vmram_sum, variables)
hardware_limit(hwcpu, vmcpu, vmcpu_sum, variables)

print('* todas maquinas virtuais precisam estar alocadas.')
constraint = ""
for j in range(k):
    for i in range(n):
        constraint += f"+1 x{variables[j][i]} "
    constraint += ">= 1;\n"
print(constraint, end='')

print('* uma maquina virtual pode estar alocada em apenas um hardware.')
constraint = ""
for j in range(k):
    for i in range(n):
        constraint += f"+1 ~x{variables[j][i]} "
    constraint += f">= {n-1};\n"
print(constraint, end='')

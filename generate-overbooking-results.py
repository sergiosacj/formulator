import os

overbooking = ['1.2', '1.5']

data = {'time': [], 'test-case': [], 'overbooking': []}

html = '<table width="500" border="1px"><tr><th>' + '</th><th>'.join(data.keys()) + '</th></tr>'

for o in overbooking:
  outputs_files = os.listdir(f'testes-overbooking/outputs/{o}')
  for filename in outputs_files:
    with open(f'testes-overbooking/outputs/{o}/{filename}', 'rb') as f:
      for line in f:
        line = line.decode('ascii')
        if line[0] == 'c' and line[2:6] == 'Time':
          data['test-case'].append(filename)
          data['time'].append(line[line.find(':')+2:line.find('s')+1])
          data['overbooking'].append(o)

for row in zip(*data.values()):
    html += '<tr><td width="101" height="40">' + '</td><td>'.join(row) + '</td></tr>'

html += '</table>'

os.system("rm results-overbooking.html")
os.system(f'echo "{html}" >> results-overbooking.html')

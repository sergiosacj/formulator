import os

data = {'time': [], 'test-case': []}

html = '<table width="500" border="1px"><tr><th>' + '</th><th>'.join(data.keys()) + '</th></tr>'

outputs_files = os.listdir(f'outputs/clasp/dinf-outputs/gpu1')

for filename in outputs_files:
    with open(f'outputs/clasp/dinf-outputs/gpu1/{filename}', 'rb') as f:
        for line in f:
            line = line.decode('ascii')
            if line[0] == 'c' and line[2:6] == 'Time':
                data['test-case'].append(filename)
                data['time'].append(line[line.find(':')+2:line.find('s')+1])

for row in zip(*data.values()):
    html += '<tr><td width="101" height="40">' + '</td><td>'.join(row) + '</td></tr>'

html += '</table>'

os.system("rm results.html")
os.system(f'echo "{html}" >> results.html')

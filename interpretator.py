n = int(input())
k = int(input())


def find(matrix, val):
    for k in range(len(matrix)):
        for j in range(len(matrix[k])):
            if matrix[k][j] == val:
                return (k, j)


variables = [list(range(1 + n * i, 1 + n * (i + 1))) for i in range(k)]
for i in range(n):
    for j in range(k):
        variables[j][i] += n

filename = input()

output_variables = []
with open(filename, 'rb') as f:
    for line in f:
        line = line.decode('ascii')
        if line[0] == 'v':
            output_variables += line[1:].split()

print('Máquinas ligadas:')
for i in range(n+1):
    if output_variables[i][0] != '-':
        print(f" hw{i+1}")

print('VMs por máquinas:')
for i in range(n, n+n*k):
    if output_variables[i][0] == '-':
        continue
    vmj, hwi = find(variables, i+1)
    print(f"VM {vmj+1} está no HW {hwi+1}")

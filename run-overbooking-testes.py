import os

input_files = []

rootdir = 'pbfvmc-resources/pbfvmc-generator/paper'
for file in os.listdir(rootdir):
    d = os.path.join(rootdir, file)
    if os.path.isdir(d):
      entries = os.listdir(d)
      for tests in entries:
        input_files.append(f'{d}/{tests}')

overbooking = ['1.2', '1.5']

for o in overbooking:
  for f in input_files:
      filename = f[f.rfind("/")+1:]
      print(filename)
      os.system(f'OVERBOOKING={o} python3 formulator.py < {f} > testes-overbooking/inputs/{o}/{filename}')
      os.system(f'timeout 300s clasp < testes-overbooking/inputs/{o}/{filename} > testes-overbooking/outputs/{o}/{filename}')
